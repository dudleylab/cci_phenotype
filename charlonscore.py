'''
 	Compute Charlson score using ICD-9 MSDW codes
'''

from lib.utility.log import logger

from datetime import datetime
import lib.utility.multicore as mc
import lib.utility.ufile as uf
import numpy as np
import sys, argparse, math
import random

log = logger(__name__)


def process (patient, cscore, timedelta = -1):
	log.info ('computing Charlson score for each patient')
	i = 1

	charlson_score = {}
	for p in patient.keys():
		if i % 100000 == 0:
			log.info ('--- processed: %d patients' % i)
		i += 1
		
		if 'mrn' not in patient[p]:
			continue

		pt = patient[p]

		vd = {}
		min_year = 10000
		
		# get valid diagnosis
		for d in pt['diag']:
			if d[1] not in cscore:
				continue
			
			year = int(d[0][:d[0].find('-')])
			vd.setdefault(d[1], set()).add(year)
			
			if year < min_year:
				min_year = year

		# set time interval
		cy = datetime.now().year 
		if timedelta == -1:
			max_year = cy
		else:
			max_year = min_year + timedelta
			if max_year > cy:
				max_year = cy
		
		# get age score
		try:
			asc = _compute_age_score (int(pt['dob']), max_year)
		except:
			asc = 0
		
		# get diagnosis score
		dsc = 0
		for d in vd:
			for y in vd[d]:
				if (y >= min_year) and (y <= max_year):
					dsc += cscore[d]
					break

		# charlson score
		csc = asc + dsc

		# 10-year survival
		z = math.pow(0.983, math.exp(csc * 0.9))

		charlson_score[pt['mrn']] = (csc, round(z, 5))

	return charlson_score



# private functions


'''
	load comorbidity file
'''
def _load_comorbidity_score (fin):
	data = uf.read_csv(fin)
	if data is None:
		return

	cscore = {}
	next(data)
	for d in data:
		cscore[d[0]] = int(d[1])
		
	return cscore



'''
	compute the age score
'''
def _compute_age_score (dob, mxy):
	age = mxy - dob

	if age <= 40:
		return 0

	if age <= 50:
		return 1

	if age <= 60:
		return 2

	if age <= 70:
		return 3

	return 4



# main function

def _process_args():
	p = argparse.ArgumentParser(description = 'Charlson Score on MSDW')
	p.add_argument(dest = 'din', help = 'input directory')
	p.add_argument(dest = 'dout', help = 'output directory')
	p.add_argument('-t', default = -1, type = int, help = 'time offset in years (default: -1 - disabled))')
	return p.parse_args(sys.argv[1:])


if __name__ == '__main__' :

	args = _process_args()
	print ''

	# create output directory
	if not uf.mkdir(args.dout):
		log.error ('impossible to create the output directory -- interrupting\n')
		sys.exit(1)
	log.info ('output directory: %s\n' % args.dout)

	# timedelta
	fout = args.dout + '/patient-charlson-score-'
	if args.t == -1:
		log.info ('time delta: all database')
		fout += 'all-db.csv'
	elif args.t == 1:
		log.info ('time delta: 1 year')
		fout += '1-year.csv'
	else:
		log.info ('time delta: %d years' % args.t)
		fout += '%d-years.csv' % args.t

	# load data
	log.info ('loading data')
	
	patient = uf.read_json (args.din + '/patient-dataset.json')
	if patient is None:
		log.error ('--- impossible to load the patient file -- interrupting')
		sys.exit(1)
	log.info ('--- loaded: %d patients' % len(patient))

	cscore = _load_comorbidity_score (args.din + '/comorbidity-score.csv')
	if cscore is None:
		log.error ('--- impossible to load the comorbidity file -- interrupting')
		sys.exit(1)
	log.info ('--- loaded: %d comorbidity scores' % len(cscore))

	# process
	print ''
	cs = process (patient, cscore, args.t)
	print ''
	log.info('obtained score values for %d patients' % len(cs))

	# saving
	h = [['Medical Record Number','Charlson Score','10-year Survival Probability']]
	od = h + [[c[0], c[1][0], '%.5f' % c[1][1]] for c in cs.items()]
	if uf.write_csv(fout, od):
		log.info ('scores saved in: ' + fout)
	else:
		log.error ('impossible to save the score file')

	print ''
	log.info('task completed\n')





	
	