'''
 	Disease Relative Risk from MSDW
'''

from lib.utility.log import logger
from itertools import combinations, product
from datetime import datetime
import lib.utility.multicore as mc
import lib.utility.ufile as uf
import numpy as np
import sys, argparse, math

log = logger(__name__)


def process (patient, icd9th = 2, timedelta = -1, nproc = 4):
	
	log.info ('initial no. of patients: %d' % len(patient))

	# organize patient by icd9
	icd9p = {}
	for p in patient:
		for d in patient[p]['diag']:
			cod = d[1]
			dt = datetime.strptime(d[0], "%Y-%m-%d").date()
			icd9p.setdefault(cod, {}).setdefault(p, set()).add(dt)

	# filter icd9 by occurrence
	for d in icd9p.keys():
		if len(icd9p[d]) < icd9th:
			del icd9p[d]
	log.info ('no. of ICD9 codes appearing in >= %d patients: %d' % (icd9th, len(icd9p)))

	# updated no. of patients
	npatient = len(set(p for it in icd9p.values() for p in it.keys()))
	log.info ('no. of processed patients: %d' % npatient)

	# get all icd9 pairs
	print ''
	ipair = sorted(combinations (icd9p.keys(), 2))
	log.info ('processing %d disease combinations' % len(ipair))

	# processing
	risk = mc.process (_wproc_icd9, [icd9p, npatient, timedelta], ipair, len(ipair), nproc)[0]

	return risk



# private functions


'''
	multicore worker to process icd9 pairs
'''
def _wproc_icd9 (dpair, icd9p, npatient, timedelta, qout):
	
	# compute disease comorbidity strength
	risk = {}
	for d in dpair:
		r = {}
		coeff = _relative_risk (d, icd9p, npatient, timedelta)
		r['pcorr'] = coeff[0]
		r['tpcorr'] = coeff[1]
		r['rrisk'] = coeff[2]
		r['conf_rr'] = coeff[3]
		r['c'] = coeff[4]
		r['pi'] = coeff[5]
		r['pj'] = coeff[6]
		risk[d] = r

	qout.put([risk])



'''
	Paerson's correlation
'''
def _relative_risk (c, icd9p, n, timedelta):
	
	pdi = icd9p[c[0]]
	pdj = icd9p[c[1]]

	pi = len(pdi)
	pj = len(pdj)
	p = float(pi * pj)
	
	# timedelta disabled
	if timedelta == -1:
		c = len(set(pdi.keys()) & set(pdj.keys()))

	# count only if timedelta is satisfied
	else:
		pco = set(pdi.keys()) & set(pdj.keys())
		c = 0
		for i in pco:
			dcomb = product (pdi[i], pdj[i])
			for d in dcomb:
				diff = abs(d[0] - d[1]).days 
				if diff <= timedelta:
					c += 1
					break

	# paerson correlation
	den = math.sqrt (p * (n - pi) * (n - pj))
	pcorr = (c * n - p) / den

	tp = (pcorr * math.sqrt(max(pi, pj) - 2)) / math.sqrt(1 - math.pow(pcorr, 2))

	if c == 0:
		return (pcorr, tp, 0.0, (0.0, 0.0), c, pi, pj)

	# risk
	rr = c * n / p

	s = 1/c + 1/p - 1/n - 1/math.pow(n,2)
	cmin = rr * math.exp(-2.56 * s)
	cmax = rr * math.exp(2.56 * s)
	
	return (pcorr, tp, rr, (cmin, cmax), c, pi, pj)



# main function

def _process_args():
	p = argparse.ArgumentParser(description = 'Disease Relative Risk on MSDW')
	p.add_argument(dest = 'din', help = 'input directory')
	p.add_argument(dest = 'dout', help = 'output directory')
	p.add_argument('-i', default = 5, type = int, help = 'min. freq. of each ICD9 in the dataset (default: 5)')
	p.add_argument('-t', default = -1, type = int, help = 'time offset in days (default: -1 - disabled))')
	p.add_argument('-p', default = 4, type = int, help = 'no. of processors (default: 4)')
	return p.parse_args(sys.argv[1:])


if __name__ == '__main__' :

	args = _process_args()

	# create output directory
	if not uf.mkdir(args.dout):
		log.error ('impossible to create the output directory -- interrupting\n')
		sys.exit(1)

	# load data
	print ''
	log.info ('loading data')
	
	patient = uf.read_json (args.din + '/patient-dataset.json')
	if patient is None:
		log.error ('--- impossible to load the patient file -- interrupting')
		sys.exit(1)

	# process
	print ''
	risk = process (patient, args.i, args.t, args.p)
	print ''

	# save as csv file
	chead = ['D1', 'D2', 'nPatientD1', 'nPatientD2', 'nCommonPatient', 'Correlation', 't-Correlation'] 
	vout = [chead + ['Relative Risk', 'Confidence Interval']]
	zout = [chead]

	for r in sorted(risk.keys()):
		it = risk[r]
		l = [r[0], r[1], it['pi'], it['pj'], it['c']]
		l.append('%.5f' % it['pcorr'])
		l.append('%.5f' % it['tpcorr'])
		
		if it['c'] == 0:
			zout.append(l)
			continue
		
		l.append('%.5f' % it['rrisk'])
		l.append('%.5f - %.5f' % it['conf_rr'])
		vout.append(l)

	if args.t == -1:
		fvout = '%s/disease-relative-risk-all-db.csv' % args.dout
		fzout = '%s/disease-relative-risk-zero-all-db.csv' % args.dout
	else:
		fvout = '%s/disease-relative-risk-%d-days.csv' % (args.dout, args.t)
		fzout = '%s/disease-relative-risk-zero-%d-days.csv' % (args.dout, args.t)

	if uf.write_csv(fvout, vout, cquote = None):
		if uf.write_csv(fzout, zout, cquote = None):
			log.info ('disease relative risk scores saved in: %s' % args.dout)
	
	print ''
	log.info ('task completed')
	print ''








