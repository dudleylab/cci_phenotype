'''
 	Compute Charlson score by year using ICD-9 MSDW codes
'''

from lib.utility.log import logger

from datetime import datetime
import lib.utility.ufile as uf
import numpy as np
import sys, argparse, math
import random

log = logger(__name__)


def process (patient, cscore, max_year = 2012, timedelta = 1):
	min_year = max_year - timedelta
	log.info ('computing Charlson score for each patient in the time range: %d - %d' % (min_year, max_year))
	
	charlson_score = {}
	i = 1
	for p in patient.keys():
		if i % 100000 == 0:
			log.info ('--- processed: %d patients' % i)
		i += 1
		
		if 'mrn' not in patient[p]:
			continue

		pt = patient[p]

		# get valid diagnosis
		vd = set()
		for d in pt['diag']:
			if d[1] not in cscore:
				continue
			
			y = int(d[0][:d[0].find('-')])
			if (y >= min_year) and (y <= max_year):
				vd.add(d[1])

		if len(vd) == 0:
			continue

		# get age score
		try:
			asc = _compute_age_score (int(pt['dob']), max_year)
		except:
			asc = 0
		
		# get diagnosis score
		dsc = 0
		for d in vd:
			dsc += cscore[d]
			
		# charlson score
		csc = asc + dsc

		# 10-year survival
		z = math.pow(0.983, math.exp(csc * 0.9))

		charlson_score[pt['mrn']] = (csc, round(z, 5))

	return charlson_score



# private functions


'''
	load comorbidity file
'''
def _load_comorbidity_score (fin):
	data = uf.read_csv(fin)
	if data is None:
		return

	cscore = {}
	next(data)
	for d in data:
		cscore[d[0]] = int(d[1])
		
	return cscore



'''
	compute the age score
'''
def _compute_age_score (dob, mxy):
	age = mxy - dob

	if age <= 40:
		return 0

	if age <= 50:
		return 1

	if age <= 60:
		return 2

	if age <= 70:
		return 3

	return 4



# main function

def _process_args():
	p = argparse.ArgumentParser(description = 'Charlson Score on MSDW')
	p.add_argument(dest = 'din', help = 'input directory')
	p.add_argument(dest = 'dout', help = 'output directory')
	p.add_argument('-m', default = 2012, type = int, help = 'last year of available data')
	p.add_argument('-t', default = 1, type = int, help = 'backward time offset in years from the last year')
	return p.parse_args(sys.argv[1:])


if __name__ == '__main__' :

	args = _process_args()
	print ''

	# create output directory
	if not uf.mkdir(args.dout):
		log.error ('impossible to create the output directory -- interrupting\n')
		sys.exit(1)
	log.info ('output directory: %s\n' % args.dout)

	# timedelta
	if args.t < 1:
		log.error ('invalid timedelta: %d -- interrupting' % args.t)
		sys.exit(1)
	fout = args.dout + '/patient-charlson-score-last-'
	if args.t == 1:
		log.info ('time delta: 1 year')
		fout += '1-year.csv'
	else:
		log.info ('time delta: %d years' % args.t)
		fout += '%d-years.csv' % args.t

	# load data
	log.info ('loading data')
	
	patient = uf.read_json (args.din + '/patient-dataset.json')
	if patient is None:
		log.error ('--- impossible to load the patient file -- interrupting')
		sys.exit(1)
	log.info ('--- loaded: %d patients' % len(patient))

	cscore = _load_comorbidity_score (args.din + '/comorbidity-score.csv')
	if cscore is None:
		log.error ('--- impossible to load the comorbidity file -- interrupting')
		sys.exit(1)
	log.info ('--- loaded: %d comorbidity scores' % len(cscore))

	# process
	print ''
	cs = process (patient, cscore, args.m, args.t)
	print ''
	log.info('obtained score values for %d patients' % len(cs))

	# saving
	h = [['Medical Record Number','Charlson Score','10-year Survival Probability']]
	od = h + [[c[0], c[1][0], '%.5f' % c[1][1]] for c in cs.items()]
	if uf.write_csv(fout, od):
		log.info ('scores saved in: ' + fout)
	else:
		log.error ('impossible to save the score file')

	print ''
	log.info('task completed\n')

	



	
	